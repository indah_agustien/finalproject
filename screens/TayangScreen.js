import React, { Component } from 'react';
import { View , StyleSheet,Text} from 'react-native';

import ImageSlider from 'react-native-image-slider';

export default class TayangScreen extends Component {
  constructor(){
    super();
    this.state={
        images:[
  require('./images/h1.jpg'),
  require('./images/h2.jpg'),
  require('./images/h3.jpg'),
  require('./images/h4.jpg'),
  require('./images/h5.jpg'),
  
        ]
    }
  }

  render() {
    return (
      <View style={{flex:1}}> 
        <View>     
      </View>
      <Text style={{marginLeft:70}}>Updated Film Terbaru, 10 juli 2020  </Text>
    
      <View style={{height:135}}>
    
      <ImageSlider 
       images={this.state.images}
       autoPlayWithInterval={3000}
    
      />
      </View>
            <Text style={{marginLeft:60, marginBottom:10, marginTop: 5}}>
          Source: www.ensiklofilmindonesia.com </Text>
          
          <View style={{marginTop:-5}}>
           <Text style={{marginLeft:100, marginBottom:5, 
            fontWeight: "bold", fontSize: 18}}>JADWAL TAYANG</Text> 
           <View style={{ borderBottomColor: 'black',
        borderBottomWidth: 1, marginTop: -5}}>
           <Text style={{fontWeight: "bold"}}>EnsikloFilm Cabang Jakarta</Text>
           <Text>Senin s/d Minggu</Text>
           <Text>Pukul 07.00 - 09.00        16.00 - 18.00        20.00-22.00</Text>
           </View>
           <View style={{ borderBottomColor: 'black',
        borderBottomWidth: 1,}}>
           <Text style={{fontWeight: "bold"}}>EnsikloFilm Cabang Medan</Text>
           <Text>Senin s/d Minggu</Text>
           <Text>Pukul 07.00 - 09.00        16.00 - 18.00        20.00-22.00</Text>
           </View>
           <View style={{ borderBottomColor: 'black',
        borderBottomWidth: 1,}} >
           <Text style={{fontWeight: "bold"}}>EnsikloFilm Cabang Bandung</Text>
           <Text>Senin s/d Minggu</Text>
           <Text>Pukul 07.00 - 09.00        16.00 - 18.00        20.00-22.00</Text>
           </View>
           <View>
           </View>
          </View>

</View>
    
    );
  }
}

const styles = StyleSheet.create({
  logo:{
     flex: 0,
      width: "60%",
      height: 300,
      justifyContent:"center",
      alignItems:"center",
  marginLeft:75
         
  }
})

