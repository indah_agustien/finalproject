import React, { Component } from 'react';
import {View,Text,StyleSheet, TouchableOpacity} from 'react-native';
import * as firebase from 'firebase';

import HalamanIsi from './HalamanIsi';

export default class HomeScreen extends Component{

    state = {
        email: "",
        displayName: ""
    }
    componentDidMount () {
        const {email,displayName} = firebase.auth().currentUser
        this.setState({email,displayName});
    }

    signOutUser = () => {
        firebase.auth().signOut();
    };

    render(){
        return(
            <View style={styles.container}>
               <View>
                <TouchableOpacity style={{marginTop: 15}} onPress={this.signOutUser}>
                <HalamanIsi />
                    <Text style={{marginLeft:300, fontWeight:"bold", color:"#003366"}}>LOG OUT</Text>
                    
                </TouchableOpacity>
                </View>
               
            </View>
        )
    }
};

const styles=StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        justifyContent:'center',
        alignItems: 'center'
    }
})