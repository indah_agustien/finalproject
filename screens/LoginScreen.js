import React, { Component } from 'react';
import {View,Text,StyleSheet,TextInput,TouchableOpacity,Image} from 'react-native';
import * as firebase from 'firebase';

export default class LoginScreen extends Component{
    
    state = {
        email: "",
        password: "",
        errorMessage: null,
    };

    handleLogin = () =>{
        const {email,password} = this.state

        firebase
        .auth()
        .signInWithEmailAndPassword(email,password)
        .catch(error => this.setState({errorMessage:error.message}));
    };
    render(){
        return(
            <View style={styles.container}>
                
                <Image source={require('./images/ensi.png')}
                 style={{width: 165,height: 165,
         borderRadius: 50,
         marginLeft: 110,
         marginTop: 15
         }}/>
                <View style={styles.errorMessage}>
                {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
            </View>
            <View style={styles.form}>
                <View>
                    <Text style={styles.inputTitle}>Email Address</Text>
                    <TextInput 
                    style={styles.input} 
                    autoCapitalize="none"
                    onChangeText={email => this.setState({email})}
                    value={this.state.email}
                    ></TextInput>
                </View>
                <View style={{marginTop:32}}>
                    <Text style={styles.inputTitle}>password</Text>
                    <TextInput
                     style={styles.input} 
                     secureTextEntry 
                     autoCapitalize="none"
                     onChangeText={password => this.setState({password})}
                    value={this.state.password}
                     
                     ></TextInput>
                </View>
            </View>
            <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
                <Text style={{color:"#fff" , fontWeight: "500"}}>Sign in</Text>
            </TouchableOpacity>
            
            </View>

            
        );
    }
}

const styles=StyleSheet.create({
    container: {
        flex: 1
    },

    greeting: {
        marginTop: 32,
        fontSize: 18,
        fontWeight: "400",
        textAlign: "center"
    },

    errorMessage: {

        height: 72,
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 30

    },

    error: {
       color: "#E9446A" ,
       fontSize: 13,
       fontWeight: "600",
       textAlign: "center"
    },

    form : {
        marginBottom: 48,
        marginHorizontal: 30,
    },

    inputTitle: {
        color:"#8A8F9E",
        fontSize: 10,
        textTransform: "uppercase",
        marginTop: -10
    },

    input : {
        borderBottomColor: "#8A8F9E",
        borderBottomWidth: StyleSheet.hairlineWidth,
        height: 40,
        fontSize: 15,
        color: "#161f3d"
    },
    button: {
        marginHorizontal: 30,
        backgroundColor: "#3EC6FF",
        borderRadius: 4,
        height: 52,
        alignItems: "center",
        justifyContent: "center",
        
    }
})