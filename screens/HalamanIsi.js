import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import TayangScreen from './TayangScreen';
import DataFilm from './DataFilm';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Profile from './Profile';

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator style={styles.cont}>
      <Tab.Screen name="Profile" component={Profile}
      style={{marginTop: 10}}
      
      />
      <Tab.Screen name="TayangScreen" component={TayangScreen} />
      <Tab.Screen name="DataFilm" component={DataFilm} />
    </Tab.Navigator>
  );
}

export default function HalamanIsi() {
  return (
    <NavigationContainer>
      <MyTabs />
    </NavigationContainer>
  );
}

const styles=StyleSheet.create({
  container: {
     
      justifyContent:'center',
      alignItems: 'center',
      marginBottom:50
  }
})