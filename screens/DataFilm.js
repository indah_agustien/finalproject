import React, { useState } from 'react';
import {StyleSheet,Text,View,TextInput, ScrollView,Image,TouchableHighlight, Modal} from 'react-native';
import axios from 'axios';
export default function App() {
  const apiurl = "http://www.omdbapi.com/?i=tt3896198&apikey=e3dda393"
  const [state,setState] = useState ({
    s: "silahkan cari film disini...",
    results: [],
    selected: {},
  });

  const search  = () => {

    axios(apiurl + "&s=" + state.s).then(({data}) => {
      let results = data.Search;
      setState(prevState => {
        return{...prevState, results: results}
      })
    })

  }
  
  const openPopup = id => {
    axios(apiurl + "&i=" + id).then(({data}) => {
      let result = data;
      setState(prevState => {
        return{...prevState, selected: result}
      });
    });
  }

    return(
      <View style={styles.container}>
        <Image source={require('./images/ensi.png')}
                 style={{width: 120,height: 120,
         borderRadius: 50,
         marginLeft: 10,
         marginTop: 10,
         marginBottom: 5
         }}/>
        <TextInput
        
        style={styles.searchbox}
        onChangeText={text => setState(prevState => {
          return{... prevState, s: text}
        }
          )}
          onSubmitEditing={search}
        value={state.s}
         />
        <ScrollView style={styles.results}>
        {state.results.map(result => (

          <TouchableHighlight key={result.imdbID}  onPress={() => openPopup(result.imdbID)}>

          <View style={styles.result}>
            <Image
            
            source={{uri: result.Poster}}
            style={{
              width: "100%",
              height: 300
            }}
            resizeMode="cover"
            />
          <Text style={styles.heading}>{result.Title}</Text>

          </View>
          </TouchableHighlight>
           ))}
        </ScrollView>
        <Modal
          animationType="fade"
          transparent={false}
          visible={(typeof state.selected.Title != "undefined")}
          >
            
            <View style={styles.popup}>
            <Text style={styles.poptitle}>
            {state.selected.Title}
            </Text>
            <Text style={{marginBottom:20}}>
              Rating: {state.selected.imdbRating}
            </Text>
            <Text>{state.selected.plot}</Text>
            </View>
            <TouchableHighlight
              onPress={() => setState(prevState => {
                return {...prevState, selected: {} }
              })}
              >
              <Text style={styles.closeBtn}>Close</Text>
            </TouchableHighlight>
        </Modal>
      </View>
    );
  }

  const styles = StyleSheet.create({
    container : {
      flex:1,
      backgroundColor: '#3EC6FF',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: -5,
      paddingHorizontal: 20,
    },

    title: {
      color: '#fff',
      fontSize: 32,
      fontWeight: '700',
      textAlign: 'center',
      marginBottom: 20

    },

    searchbox: {
      fontSize: 20,
      fontWeight: '300',
      padding: 20,
      width: '100%',
      backgroundColor: '#fff',
      borderRadius: 8,
      marginBottom: 40,
    },

    results: {
      flex: 1,
    },

    results: {
      flex: 1,
      width: '100%',
      marginBottom: 20,
    },

    heading: {
      color: '#fff',
      fontSize: 18,
      fontWeight: '700',
      padding: 20,
      backgroundColor: '#445565'
    
    },
    popup: {
      padding: 20,
    },

    poptitle: {
      fontSize: 24,
      fontWeight: '700',
      marginBottom: 5
    },

    closeBtn: {
      padding: 20,
      fontSize: 24,
      color: '#fff',
      fontWeight: '700',
      backgroundColor: '#2484c4',
    }


  });

