import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoadingScreen from './screens/LoadingScreen';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';

import * as firebase from 'firebase';


var firebaseConfig = {
  apiKey: "AIzaSyDpRBBSw7fWdKwSm0JELmlgSmkw85N9Obs",
  authDomain: "jaldata.firebaseapp.com",
  databaseURL: "https://jaldata.firebaseio.com",
  projectId: "jaldata",
  storageBucket: "jaldata.appspot.com",
  messagingSenderId: "7863474347",
  appId: "1:7863474347:web:a46281e0cb3bed60927fee",
  measurementId: "G-3F07SNR9WT"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const AppStack = createStackNavigator({
  Home: HomeScreen
})

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  
})

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: LoadingScreen,
      App: AppStack,
      Auth: AuthStack
    },
    {
      initialRouteName: "Loading"
    }
  )
)